class MoviesController < ApplicationController

  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  end

  def index

    @all_ratings = Movie.all_ratings
  
    if (!params[:sort].nil? || !params[:back_to_index].nil?)
      params[:ratings] = session[:ratings_]
      if (!params[:back_to_index].nil?)
        params[:sort] = session[:sort_]
      end
      session.clear
    end
    
    #debugger

    if @selected_ratings.nil? && params[:ratings].nil?
      if session[:ratings_].nil?
        @selected_ratings = @all_ratings
        @sort = params[:sort]
        @movies = Movie.order(@sort)
      else
        params[:ratings] = session[:ratings_]
      end
    end
    if !params[:ratings].nil? 
      params[:ratings].each_key do |rating|
        (@selected_ratings ||= [] )<< rating
      end
        @sort = params[:sort]
        @movies = Movie.order(@sort).where(:rating => params[:ratings].keys)
        session[:ratings_] = params[:ratings]
        session[:sort_] = params[:sort]
    end
   # debugger
  end

  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

end
